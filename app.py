import re
import json
from flask import Flask, jsonify, request, url_for, make_response
from flask_cors import CORS
import pandas as pd


SKIPTOKEN_REGEX = re.compile(r'skip-(?P<skip>\d+)_return-(?P<top>\d+)')
# tuple of columns that are always present in data set
MANDATORY_COLUMNS = ('@odata.type',)
# pagination, defaul value
RECORDS_PER_PAGE = 100
# JSON file with data to be served
with open('data.json', 'r') as data_file:
    DATA = json.load(data_file)

app = Flask(__name__)
CORS(app)


def create_skiptoken(number_to_skip, number_to_return):
    '''Creates a string to be added to nextLink as a $skipToken.
    
    Parameters
    ----------
    number_to_skip, number_to_return : int
        Number of records to skip and return in response, respectively.

    Returns
    -------
    str
        A mock skip token.
    '''
    return f'skip-{number_to_skip}_return-{number_to_return}'

def read_skiptoken(skip_token):
    '''Parses $skipToken value to a $skip and $top parameters.
    
    Parameters
    ----------
    skip_token : str
    
    Returns
    -------
    dict
        Dictionary with
    '''
    m = SKIPTOKEN_REGEX.match(skip_token)
    if m is None:
        raise ValueError('Could not match skiptoken')
    return {
        '$skip': m.group('skip'),
        '$top': m.group('top')
    }

def select_columns(data_set, selected_columns, mandatory_columns=None):
    '''Returns a subset of columns in dataset.

    Parmeters
    ---------
    data_set : list of dicts
    selected_columns : list-like
    mandatory_columns : list-like, optional

    Returns
    -------
    list of dicts
    '''
    df = pd.DataFrame(data_set)
    if mandatory_columns is not None:
        selected_columns = tuple([*mandatory_columns, *selected_columns])
    selected_columns = set(selected_columns)
    # make sure that we use only columns that are in dataset
    selected_columns = set(df.columns).intersection(selected_columns)
    df_out = df[list(selected_columns)]
    # fix default dataframe behaviour
    df_out = df_out.replace({pd.np.nan: None})

    return df_out.to_dict('records')


def filter_data(data_set, params_dict):
    '''Filers data based on given query params.

    The filter are applied in the following order:
         - skiptoken
         - skip
         - top

    Parameters
    ----------
    data_set : list
        A list of dictionaries as read from JSON file.
    params_dict : dict-like
        Parameters to be applied.
        
    Returns
    -------
    tuple
        Filtered data set (list) and skip_token (str or None).
    '''
    params_dict = params_dict.to_dict(flat=True)
    skip_token = None
    # in case no args were provided
    if len(params_dict.keys()) == 0:
        if RECORDS_PER_PAGE < len(DATA):
            skip_token = create_skiptoken(RECORDS_PER_PAGE, RECORDS_PER_PAGE)
        return data_set[:RECORDS_PER_PAGE], skip_token

    # replace $skip and $top with values from skip token (if there is one)
    if '$skiptoken' in params_dict.keys():
        print(f'TOKEN: {params_dict["$skiptoken"]}')
        skiptoken_data = read_skiptoken(params_dict['$skiptoken'])
        params_dict['$skip'] = skiptoken_data['$skip']
        params_dict['$top'] = skiptoken_data['$top']

    filtered_data_set = []
    
    skip = 0
    # skip some rows
    if '$skip' in params_dict.keys():
        skip = int(params_dict['$skip'])
        filtered_data_set = data_set[skip:]
    else:
        filtered_data_set = data_set
    
    data_set_length = len(filtered_data_set)
    # return given number of rows
    if '$top' in params_dict.keys():
        top = int(params_dict['$top'])
        filtered_data_set = filtered_data_set[:top]
        if data_set_length - top > 0:
            skip_token = create_skiptoken(skip + top, top)
    else:
        filtered_data_set = filtered_data_set[:RECORDS_PER_PAGE]
        skip_token = create_skiptoken(skip + RECORDS_PER_PAGE, RECORDS_PER_PAGE)

    # return requested columns
    if '$select' in params_dict.keys():
        requested_columns = params_dict['$select'].split(',')
        filtered_data_set = select_columns(
            filtered_data_set, requested_columns,
            mandatory_columns=MANDATORY_COLUMNS
        )

    # clean-up: remove fields with None value for "#microsoft.graph.group" records
    for record in filtered_data_set:
        if record['@odata.type'] == '#microsoft.graph.group':
            record.pop('accountEnabled')

    return filtered_data_set, skip_token

@app.route('/')
def serve_data():
    '''App's only endpoint. Returns data filtered according to query params.
    
    Available params:
        - $skip - skip the given number of records
        - $top - return only a given number of top-most records
        - $skiptoken - combination of $skip and $top used for pagination
        - $select - list of properties to be included in response (string of comma separated names)
    '''
    requested_data, skip_token = filter_data(DATA, params_dict=request.args)

    args = ''
    # build query string with parameters that were present in original request
    # used for context and nextLink URLs
    args_list = ['='.join((k, v)) for k, v in request.args.items() if 'skiptoken' not in k]
    if len(args_list) > 0:
        args += '?' + '&'.join(args_list)

    response_data = {
        '@odata.context': url_for('serve_data', _external=True) + args,
        'value': requested_data
    }
    if skip_token is not None:
        if args != '':
            args += '&'
        else:
            args += '?'
        # add skip token itself
        args += '$skiptoken=' + skip_token
        response_data['@odata.nextLink'] = url_for('serve_data', _external=True) + args
    resp = make_response(response_data)
    # copied from Graph response
    resp.mimetype = 'application/json;odata.metadata=minimal;odata.streaming=true;IEEE754Compatible=false;charset=utf-8'

    return resp
