# Local MS Graph API simulator

## Setup
1. Create virtual environment in project's directory with
```shell
python -m venv venv
```
2. Activate it, on Windows:
```cmd
> venv\Scripts\activate
```
3. Install dependencies:
```shell
pip install -r requirements.txt
```
4. Copy _data.json_ file to the projects directory. The file **must** be a list of objects.

## Run
In project's direcotry, activate virtual environment (Windows):
```cmd
> venv\Scripts\activate
```
then type
```
flask run
```
and hit return.

## Usage
Only GET request is available under http://localhost:5000/ with the following parameters:
- $skip (integer)
- $top (integer)
- $select (string of comma-separated column names)
- $skiptoken (used for pagination)
